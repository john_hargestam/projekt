package com.miun.joha1221.todos;

import java.util.Calendar;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

public class DatePickerDialogFragment extends DialogFragment {
	
	private OnDateSetListener callback;
	private Calendar calendar;
	
	public DatePickerDialogFragment() {
		calendar = Calendar.getInstance();
	}
	
	public void setCallback(OnDateSetListener callback) {
		this.callback = callback;
	}
	
	public void setDate(long timeInMillis) {
		calendar.setTimeInMillis(timeInMillis);
	}
 
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {		
		return new DatePickerDialog(getActivity(), callback,
				calendar.get(Calendar.YEAR), 
				calendar.get(Calendar.MONTH),
				calendar.get(Calendar.DAY_OF_MONTH));
	}
}
