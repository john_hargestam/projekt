package com.miun.joha1221.todos;

public class Todo {
	
	private String title;
	private String description;
	private long deadline;
	private boolean hasDeadline;
	
	public Todo(String title, String description, long deadline, boolean hasDeadline) {
		this.title = title;
		this.description = description;
		this.deadline = deadline;
		this.hasDeadline = hasDeadline;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public void setDeadline(long deadline) {
		this.deadline = deadline;
	}
	
	public void setHasDeadline(boolean hasDeadline) {
		this.hasDeadline = hasDeadline;
	}
	
	public String getTitle() {
		return title;
	}
	
	public String getDescription() {
		return description;
	}

	public long getDeadline() {
		return deadline;
	}

	public boolean hasDeadline() {
		return hasDeadline;
	}
	
}
