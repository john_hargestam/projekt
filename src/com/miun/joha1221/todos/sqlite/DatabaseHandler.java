package com.miun.joha1221.todos.sqlite;

import java.text.SimpleDateFormat;
import java.util.Locale;

import com.miun.joha1221.todos.Todo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.preference.PreferenceManager;

public class DatabaseHandler extends SQLiteOpenHelper {

	public static final String TITLE = "title";
	public static final String DESCRIPTION = "description";
	public static final String DEADLINE = "deadline";
	public static final String HASDEADLINE = "hasDeadline";
	public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyy/MM/dd", Locale.getDefault());
	public static final SimpleDateFormat TIME_FORMAT = new SimpleDateFormat("HH:mm", Locale.getDefault());
	public static final String[] SORTING_TYPES = {"Time added", "Deadline"};

	private static final String DATABASE_NAME = "dbtodo.db";
	private static final int DATABASE_VERSION = 1;
	private static final String TODO_TABLE = "todo";
	private static final String ORDER_PREF_KEY = "orderPreference";
	private static DatabaseHandler instance;
	
	private SQLiteDatabase db;
	private Context context;
	private String order;

	public static DatabaseHandler getInstance(Context context) {
		if (instance == null) {
			instance = new DatabaseHandler(context.getApplicationContext());
		}
		return instance;
	}

	private DatabaseHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		this.context = context;
		this.db = getWritableDatabase();
		this.order = PreferenceManager.getDefaultSharedPreferences(context)
				.getString(ORDER_PREF_KEY, "_id");
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("DROP TABLE IF EXISTS " + TODO_TABLE);
		db.execSQL("create table " + TODO_TABLE + " ("
				+ "_id INTEGER PRIMARY KEY AUTOINCREMENT," + TITLE
				+ " TEXT," + DESCRIPTION + " TEXT," + DEADLINE
				+ " INTEGER," + HASDEADLINE + " INTEGER);");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + TODO_TABLE);
		onCreate(db);
	}

	public long insertTodo(Todo todo) {
		ContentValues values = new ContentValues();
		values.put(TITLE, todo.getTitle());
		values.put(DESCRIPTION, todo.getDescription());
		values.put(DEADLINE, todo.getDeadline());
		values.put(HASDEADLINE, todo.hasDeadline() ? 1 : 0);

		return db.insert(TODO_TABLE, null, values);
	}

	public void updateTodo(Todo todo, long id) {
		ContentValues values = new ContentValues();
		values.put(TITLE, todo.getTitle());
		values.put(DESCRIPTION, todo.getDescription());
		values.put(DEADLINE, todo.getDeadline());
		values.put(HASDEADLINE, todo.hasDeadline() ? 1 : 0);

		db.update(TODO_TABLE, values, "_id=" + id, null);
	}
	
	public Todo getTodo(long id) {
		Cursor c = db.rawQuery("SELECT * FROM " + TODO_TABLE +
				" WHERE _id=" + id, null);
		if (!c.moveToFirst()) {
			return null;
		}
		String title = c.getString(c.getColumnIndex(TITLE));
		String description = c.getString(c.getColumnIndex(DESCRIPTION));
		long deadline = c.getLong(c.getColumnIndex(DEADLINE));
		boolean hasDeadline = c.getInt(c.getColumnIndex(HASDEADLINE)) == 1;
		
		return new Todo(title, description, deadline, hasDeadline);
	}

	public Cursor getAllTodosAsCursor() {
		return db.rawQuery("SELECT * FROM " + TODO_TABLE + " ORDER BY " + order, null);
	}
	
	public void removeAllTodos() {
		db.delete(TODO_TABLE, null, null);
	}

	public void sortBy(String sortingType) {
		if ("Time added".equals(sortingType)) {
			order = "_id";
		} else if ("Deadline".equals(sortingType)) {
			order = HASDEADLINE + " DESC, " + DEADLINE;
		} else {
			return;
		}
		PreferenceManager.getDefaultSharedPreferences(context).edit()
				.putString(ORDER_PREF_KEY, order).commit();
		
	}
}
