package com.miun.joha1221.todos;

import java.util.Calendar;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

public class TimePickerDialogFragment extends DialogFragment {

	OnTimeSetListener callback;
	Calendar calendar;
	
	public TimePickerDialogFragment() {
		calendar = Calendar.getInstance();
	}
	
	public void setCallback(OnTimeSetListener callback) {
		this.callback = callback;
	}
	
	public void setTime(long timeInMillis) {
		calendar.setTimeInMillis(timeInMillis);
	}
 
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		return new TimePickerDialog(getActivity(), callback,
				calendar.get(Calendar.HOUR_OF_DAY),
				calendar.get(Calendar.MINUTE), true);
	}
}
