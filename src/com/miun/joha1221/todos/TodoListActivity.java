package com.miun.joha1221.todos;

import static com.miun.joha1221.todos.sqlite.DatabaseHandler.*;

import java.util.Calendar;

import com.miun.joha1221.todos.sqlite.DatabaseHandler;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class TodoListActivity extends ListActivity implements LoaderCallbacks<Cursor> {
	
	public final static String ID_EXTRA = "_id";
	public final static String POSITION_EXTRA = "position";
	
	private CursorAdapter adapter;
	private AlertDialog sortByDialog;
	private AlertDialog clearDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_todolist);
		
		adapter = new TodoCursorAdapter(this, null, 0);
		setListAdapter(adapter);
		
		getLoaderManager().initLoader(0, null, this);
		
	}
	
	private class TodoCursorAdapter extends CursorAdapter {
		
		private LayoutInflater inflater;

		public TodoCursorAdapter(Context context, Cursor c, int flags) {
			super(context, c, flags);
			inflater = LayoutInflater.from(context);
		}

		@Override
		public void bindView(View view, Context context, Cursor cursor) {
			TextView titleTV = (TextView)view.findViewById(R.id.title);
			titleTV.setText(cursor.getString(cursor.getColumnIndex(TITLE)));
			
			TextView descriptionTV = (TextView)view.findViewById(
					R.id.description);
			descriptionTV.setText(cursor.getString(cursor.getColumnIndex(
					DESCRIPTION)));
			
			//insert icons here
			
			boolean hasDeadline = cursor.getInt(cursor.getColumnIndex(
					HASDEADLINE)) == 1;
			TextView deadlineTV = (TextView)view.findViewById(R.id.deadline);
			if (hasDeadline) {
				//display dynamic deadline
				long deadline = cursor.getLong(cursor.getColumnIndex(DEADLINE));
				Calendar cal = Calendar.getInstance();
				cal.setTimeInMillis(deadline);
				
				int todayDay = Calendar.getInstance().get(Calendar.DAY_OF_YEAR);
				int deadlineDay = cal.get(Calendar.DAY_OF_YEAR);
				int daysDiff = deadlineDay - todayDay;
				String time = TIME_FORMAT.format(cal.getTime());
				StringBuilder builder = new StringBuilder();
				
				if (daysDiff < 0) {
					if (daysDiff == -1) {
						builder.append("Overdue 1 day");
					} else {
						builder.append("Overdue ").append(-daysDiff)
								.append(" days");
					}
					deadlineTV.setTextColor(getResources().getColor(R.color.red));
				} else {
					if (daysDiff == 0) {
						builder.append("Today at");
					} else if (daysDiff == 1) {
						builder.append("Tomorrow at");
					} else {
						builder.append(DATE_FORMAT.format(cal.getTime()));
					}
					deadlineTV.setTextColor(getResources().getColor(
							android.R.color.black));
					builder.append(' ').append(time);
				}
				deadlineTV.setText(builder.toString());
			} else {
				deadlineTV.setText("");
			}
		}

		@Override
		public View newView(Context context, Cursor cursor, ViewGroup parent) {
			return inflater.inflate(R.layout.todo_list_item, parent, false);
		}
	}
	
	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		CursorLoader loader = new CursorLoader(this,
				null, null, null, null, null) {
			@Override
			public Cursor loadInBackground() {
				return DatabaseHandler.getInstance(getContext())
						.getAllTodosAsCursor();
			}
		};
		return loader;
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
		adapter.swapCursor(cursor);
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
		adapter.swapCursor(null);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		//create dialog for sorting type
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(getString(R.string.action_sortby));
		builder.setItems(SORTING_TYPES, new DialogInterface.OnClickListener() {
			@Override
		    public void onClick(DialogInterface dialog, int item) {
		    	SortBy(SORTING_TYPES[item]);
		    }
		});
		sortByDialog = builder.create();
		
		//create dialog for clearing todos
		builder = new AlertDialog.Builder(this);
		builder.setTitle(getString(R.string.action_clear));
		builder.setMessage(getString(R.string.clear_question));
		builder.setPositiveButton(getString(android.R.string.ok), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				clearTodos();				
			}
		});
		builder.setNegativeButton(getString(android.R.string.cancel), null);
		clearDialog = builder.create();
		
		getMenuInflater().inflate(R.menu.todolist, menu);
		return true;
	}
	
	private void SortBy(String sortingType) {
		DatabaseHandler.getInstance(getBaseContext()).sortBy(sortingType);
		getLoaderManager().restartLoader(0, null, this);
	}
	
	private void clearTodos() {
		DatabaseHandler.getInstance(getBaseContext()).removeAllTodos();
		getLoaderManager().restartLoader(0, null, this);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_newtodo:
			Intent intent = new Intent(this, EditTodoActivity.class);
			startActivityForResult(intent, 0);
			break;
		case R.id.action_sortby:
			sortByDialog.show();
			break;
		case R.id.action_clear:
			clearDialog.show();
			break;
		}
		return true;
	}
	
	@Override
	protected void onListItemClick(ListView listView, View view, int position, long id) {
		Intent intent = new Intent(this, EditTodoActivity.class);
		intent.putExtra(ID_EXTRA, id);
		startActivityForResult(intent, 0);
    }
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_OK) {
			getLoaderManager().restartLoader(0, null, this);
		}
	}

}
