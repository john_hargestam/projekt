package com.miun.joha1221.todos;

import static com.miun.joha1221.todos.TodoListActivity.*;
import static com.miun.joha1221.todos.sqlite.DatabaseHandler.*;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import com.miun.joha1221.todos.sqlite.DatabaseHandler;

import android.support.v7.app.ActionBarActivity;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;

public class EditTodoActivity extends ActionBarActivity implements
		OnDateSetListener, OnTimeSetListener {
	
	private Context context;
	private EditText titleET;
	private EditText descriptionET;
	private CheckBox deadlineCB;
	private Button deadlineDateBtn;
	private Button deadlineTimeBtn;
	private Calendar calendar;
	private SimpleDateFormat displayedDateFormat;
	private long todoId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit);
		context = getBaseContext();
		
		titleET = (EditText)findViewById(R.id.et_title);
		descriptionET = (EditText)findViewById(R.id.et_description);
		deadlineCB = (CheckBox)findViewById(R.id.cb_deadline);
		deadlineDateBtn = (Button)findViewById(R.id.btn_deadline_date);
		deadlineTimeBtn = (Button)findViewById(R.id.btn_deadline_time);
		
		calendar = Calendar.getInstance();
		displayedDateFormat = new SimpleDateFormat("EEE, MMM dd, yyy", Locale.getDefault());
		
		todoId = getIntent().getLongExtra(ID_EXTRA, -1);
		if (todoId != -1) {
			Todo todo = DatabaseHandler.getInstance(context).getTodo(todoId);
			if (todo != null) {
				titleET.setText(todo.getTitle());
				descriptionET.setText(todo.getDescription());
				deadlineCB.setChecked(todo.hasDeadline());
				long deadline = todo.getDeadline();
				calendar.setTimeInMillis(deadline);
			}
		} else {
			calendar.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY) + 1);
			calendar.set(Calendar.MINUTE, 0);
		}
		
		deadlineDateBtn.setText(displayedDateFormat.format(calendar.getTime()));
		deadlineTimeBtn.setText(TIME_FORMAT.format(calendar.getTime()));
		
		if (!deadlineCB.isChecked()) {
			deadlineDateBtn.setEnabled(false);
			deadlineTimeBtn.setEnabled(false);
		}
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.edittodo, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_settings:
			
		}
		return true;
	}
	
	public void setDeadlineEnabled(View view) {
		boolean enabled = deadlineCB.isChecked();
		deadlineDateBtn.setEnabled(enabled);
		deadlineTimeBtn.setEnabled(enabled);
	}
	
	public void setDeadlineDate(View view) {
		DatePickerDialogFragment datePicker = new DatePickerDialogFragment();
		datePicker.setCallback(this);
		datePicker.setDate(calendar.getTimeInMillis());
		datePicker.show(getSupportFragmentManager(), "setDeadlineDate");
	}
	
	@Override
	public void onDateSet(DatePicker view, int year, int monthOfYear,
			int dayOfMonth) {
		calendar.set(Calendar.YEAR, year);
		calendar.set(Calendar.MONTH, monthOfYear);
		calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
		deadlineDateBtn.setText(displayedDateFormat.format(calendar.getTime()));
	}
	
	public void setDeadlineTime(View view) {
		TimePickerDialogFragment timePicker = new TimePickerDialogFragment();
		timePicker.setCallback(this);
		timePicker.setTime(calendar.getTimeInMillis());
		timePicker.show(getSupportFragmentManager(), "setDeadlineTime");
	}
	
	@Override
	public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
		calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
		calendar.set(Calendar.MINUTE, minute);
		deadlineTimeBtn.setText(TIME_FORMAT.format(calendar.getTime()));
	}
	
	public void saveTodo(View view) {
		String title = titleET.getText().toString();
		String desc = descriptionET.getText().toString();
		long deadline = calendar.getTimeInMillis();
		boolean hasDeadline = deadlineCB.isChecked();
		
		Todo todo = new Todo(title, desc, deadline, hasDeadline);
		
		if (todoId != -1) {
			DatabaseHandler.getInstance(context).updateTodo(todo, todoId);
		} else {
			DatabaseHandler.getInstance(context).insertTodo(todo);
		}
		setResult(RESULT_OK);
		finish();
	}
	
	public void cancelTodo(View view) {
		setResult(RESULT_CANCELED);
		finish();
	}

}
